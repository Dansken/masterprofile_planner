Ett personligt projekt för att se ifall jag kan göra processen att planera sin masterprofil lite smidigare.
Just nu så finns det bara några basfunktioner

	För att använda programmet så börja med att ladda in kurser från https://studieinfo.liu.se/
	file > load courses from url
	Ange url från önskat program, viktigt att ha med "https://" i url:n. 
	jag har bara testat civilingenjör i datateknik hur den fungerar för andra program är oklart
	
	Efter korrekt url så bör kurserna visas upp i listan, 
	väljer man en kurs så bör perioden den ges visas i schemat om datan från liu är ok, 
	ibland så specificerar inte liu när den ges i hemsidan och då bör inte en period visas
	
	Efter vald kurs kan man dubbelklicka på den period man vill ta kursen och då bör kurskoden visas i schemat om inte det redan finns en kurs i samma period och block,
	detta funkar dock inte för de flesta kurser som ges termin 1-6 av någon anledning
	
	Om man vill ta bort en kurs så kan man rensa hela schemat i "Edit > clear courses" alternativet att ta bort en enstaka kurs finns inte än
	
	Det går sedan att spara schemat till en txt fil, det går för tillfället inte att ladda skapade filer
