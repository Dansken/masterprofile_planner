import urllib.request
from bs4 import BeautifulSoup

def getSpecDict(page):
    spec = {}
    for child in page.find(id="specializations-filter").findChildren('option')[1:]:
        spec[child['value']] = child.get_text()
    return spec

def getCourseData(link):
    page = readPage(link)
    specs = getSpecDict(page)

    courseDict = {}
    for item in page.find_all('tr'):
        if not item.attrs or "main-row" not in item['class']: continue
        if getCode(item) in courseDict:
            course = courseDict[getCode(item)]
            if course['semester'] != getCourseTimingSemerster(item):
                course['semester'] = dictMerge(course['semester'], getCourseTimingSemerster(item))
            if course['spec'] != getCourseSpecInfo(item):
                course['spec'] = dictMerge(course['spec'], getCourseSpecInfo(item))
        else:
            courseDict[getCode(item)] = getCourseInfo(item)

    courseDict = cleanSemesterData(courseDict)
    return [courseDict, specs]


def readPage(link):
    page = urllib.request.urlopen(link)
    mybytes = page.read()
    mystr = mybytes.decode("utf8")
    page.close()
    soup = BeautifulSoup(mystr, 'html.parser')
    return soup

def getCourseInfo(item):
    course = {}

    course['name'] = getCourseName(item)
    course['link'] = getCourseLink(item)
    course['hp'] = getCourseHP(item)
    course['level'] = getCourseLevel(item)
    course['semester'] = getCourseTimingSemerster(item)
    course['spec'] = getCourseSpecInfo(item)

    return course

def getCourseName(item):
    return item.a.get_text()

def getCourseLink(item):
    return 'https://studieinfo.liu.se' + item.a['href']

def getCourseHP(item):
    return item.find_all('td')[2].text

def getCourseLevel(item):
    return item.find_all('td')[3].text

def getCode(item):
    return item['data-course-code']

def getCourseTimingSemerster(item):
    semesters = {}
    semesters[getCourseSemester(item)] = getCourseTimingPeriod(item)
    return semesters

def getCourseTimingPeriod(item):
    periods = {}
    periods[getCoursePeriod(item)] = getCourseBlock(item)
    return periods

def getCourseSemester(item):
    return item.parent.parent.parent.parent.parent.parent.header.h3.text.split()[1]

def getCoursePeriod(item):
    return item.parent.tr.text.split()[1]

def getCourseBlock(item):
    return item.find_all('td')[4].text

def getCourseSpecInfo(item):
    specs = {}
    specs[getCourseSpec(item)] = getCourseVOF(item)
    return specs

def getCourseSpec(item):
    spec = item.parent.parent.parent.parent['data-specialization']
    return spec if spec != '' else 'free'

def getCourseVOF(item):
    return item['data-vof']

def cleanSemesterData(courseDict):
    newDict = courseDict
    for course in courseDict:
        semesters = courseDict[course]['semester']
        for semester in semesters:
            periods = semesters[semester]
            if len(list(periods)) == 1 or '0' in periods: continue
            if periods['2'] == '-' and periods['1'] != '-':
                periods['2'] = periods['1']
            newDict[course]['semester'][semester] = periods
    return newDict
        

def dictMerge(a , b):
    for key in b:
        if key in a and a[key] != b[key] and isinstance(a[key], dict):
            a[key] = dictMerge(a[key], b[key])
        else:
            a[key] = b[key]
    return a
